# Maintainer: Rustmilian Rustmilian@proton.me

pkgname=('autolinux-installer'
	'autolinux-installer-qt5')

pkgver=3.3.5
pkgrel=1
pkgdesc='Distribution-independent installer framework'
arch=($CARCH)
url="https://gitlab.com/a4to/autolinux-installer"

license=('BSD-2-Clause'
	'CC-BY-4.0'
	'CC0-1.0'
	'GPL-3.0-or-later'
	'LGPL-2.1-only'
	'LGPL-3.0-or-later'
	'MIT')

depends=('ckbcomp'
	'efibootmgr'
	'gtk-update-icon-cache'
	'hwinfo'
	'icu'
	'kpmcore>=24.01.75'
	'libpwquality'
	'mkinitcpio-openswap'
	'squashfs-tools'
	'yaml-cpp')

makedepends=('extra-cmake-modules' 'git')

backup=('usr/share/autolinux-installer/modules/bootloader.conf'
	'usr/share/autolinux-installer/modules/displaymanager.conf'
	'usr/share/autolinux-installer/modules/initcpio.conf'
	'usr/share/autolinux-installer/modules/unpackfs.conf')

source=("$pkgname-$pkgver.tar.gz::$url/archive/v$pkgver.tar.gz"
	"autolinux-installer.desktop"
	"autolinux-installer_polkit"
	"49-nopasswd-autolinux-installer.rules"
	"paru-support.patch"
	"flag.patch")

sha256sums=('SKIP'
  'SKIP'
  'SKIP'
  'SKIP'
  'SKIP'
  'SKIP')

prepare() {
	makedepends() {
	if [[ $pkgname == 'autolinux-installer' ]]; then
		makedepends+=('qt6-tools' 'qt6-translations')
	elif [[ $pkgname == 'autolinux-installer-qt5' ]]; then
		makedepends+=('qt5-tools' 'qt5-translations')
	fi
	handle_qt_version
	}

	handle_qt_version() {
	if [[ $pkgname == 'autolinux-installer' ]]; then
		qt=6
		handle_qt6_base
	elif [[ $pkgname == 'autolinux-installer-qt5' ]]; then
		qt=5
	fi
	cd "${srcdir}/${pkgname}-${pkgver}" || return
		sed -i 's/"Install configuration files" OFF/"Install configuration files" ON/' "${srcdir}/${pkgname}-${pkgver}/CMakeLists.txt"
		sed -i "s|\${AUTOLINUX-INSTALLER_VERSION_MAJOR}.\${AUTOLINUX-INSTALLER_VERSION_MINOR}.\${AUTOLINUX-INSTALLER_VERSION_PATCH}|${pkgver}-${pkgrel}|g" CMakeLists.txt
		sed -i "s|AUTOLINUX-INSTALLER_VERSION_RC 1|AUTOLINUX-INSTALLER_VERSION_RC 0|g" CMakeLists.txt
		git apply --verbose ../paru-support.patch
	}

##	Non-standard ##
	handle_qt6_base() {
	if ls ./qt6-base/qt6-base-*.pkg.tar.zst 1> /dev/null 2>&1; then
		echo "qt6-base already exists"
		update_qt6_base
	else
		clone_and_build_qt6_base
	fi
	}

	update_qt6_base() {
		cd qt6-base || exit
		echo -e "\e[1;32mUpdate qt6-base? (y/n) : \e[0m\c"
		read -r input
	if [ "$input" = "y" ]; then
		git pull
		git apply --verbose ../flag.patch
		makepkg -sif
	else
		makepkg -si
	fi
	}

	clone_and_build_qt6_base() {
		git clone https://gitlab.archlinux.org/archlinux/packaging/packages/qt6-base.git
		cd qt6-base || exit
		git apply --verbose ../flag.patch
		makepkg -si
	}
##	Non-standard ##

	# Call the function to start the process
	makedepends

}

build() {
	cd "${srcdir}/${pkgname}-${pkgver}" || return
	mkdir -p build
	cd build || return
	cmake .. \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DWITH_QT"${qt}"=ON \
		-DWITH_PYTHONQT=OFF \
		-DWITH_KF5DBus=OFF \
		-DBoost_NO_BOOST_CMAKE=ON \
		-DWEBVIEW_FORCE_WEBKIT=OFF \
		-DSKIP_MODULES="webview \
						tracking \
						interactiveterminal \
						initramfs \
						initramfscfg \
						dracut \
						dracutlukscfg \
						dummyprocess \
						dummypython \
						dummycpp \
						dummypythonqt \
						services-openrc \
						keyboardq \
						localeq \
						welcomeq"
	make
}

package_autolinux-installer() {
	depends=('kconfig>=5.246'
		'kcoreaddons>=5.246'
		'ki18n>=5.246'
		'kiconthemes>=5.246'
		'kio>=5.246'
		'polkit-qt6>=0.175.0'
		'qt6-base>=6.6.0'
		'qt6-svg>=6.6.0'
		'solid>=5.246')

	cd "${srcdir}/${pkgname}-${pkgver}/build" || return
	make DESTDIR="$pkgdir" install
	install -Dm644 "${srcdir}/autolinux-installer.desktop" "$pkgdir/etc/xdg/autostart/autolinux-installer.desktop"
	install -Dm755 "${srcdir}/autolinux-installer_polkit" "$pkgdir/usr/bin/autolinux-installer_polkit"
	install -Dm644 "${srcdir}/49-nopasswd-autolinux-installer.rules" "$pkgdir/etc/polkit-1/rules.d/49-nopasswd-autolinux-installer.rules"
	chmod 750 "$pkgdir"/etc/polkit-1/rules.d

}

package_autolinux-installer-qt5() {
	depends=('kconfig5>=5.113.0'
		'kcoreaddons5>=5.113.0'
		'kiconthemes5>=5.113.0'
		'ki18n5>=5.113.0'
		'kio5>=5.113.0'
		'solid5>=5.113.0'
		'qt5-base>=5.15.11'
		'qt5-svg>=5.15.11'
		'polkit-qt5>=0.175.0'
		'plasma-framework5>=5.58'
		'qt5-xmlpatterns>=5.15.11')

	cd "${srcdir}/${pkgname}-${pkgver}/build" || return
	make DESTDIR="$pkgdir" install
	install -Dm644 "${srcdir}/autolinux-installer.desktop" "$pkgdir/etc/xdg/autostart/autolinux-installer.desktop"
	install -Dm755 "${srcdir}/autolinux-installer_polkit" "$pkgdir/usr/bin/autolinux-installer_polkit"
	install -Dm644 "${srcdir}/49-nopasswd-autolinux-installer.rules" "$pkgdir/etc/polkit-1/rules.d/49-nopasswd-autolinux-installer.rules"
	chmod 750 "$pkgdir"/etc/polkit-1/rules.d
}
