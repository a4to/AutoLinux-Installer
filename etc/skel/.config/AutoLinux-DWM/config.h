//                                      See LICENSE file for copyright and license details.                                     //


#define TERMINAL "st"
#define TERMCLASS "St"
#define TERMINAL2 "alacritty"
#define TERMCLASS2 "Alacritty"
#define BROWSER "brave"
#define MODKEY Mod4Mask
#define ALTKEY Mod1Mask
#define BACSLASH 0x5c
#define CTRLKEY ControlMask
#define NOMOD 0

#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

#define HARD CTRLKEY|ALTKEY|MODKEY
#define FUZZY CTRLKEY|ALTKEY|ShiftMask
#define IMPOSSIBLE CTRLKEY|ALTKEY|MODKEY|ShiftMask

//--------------------------------------------------------- Appearance ---------------------------------------------------------//

static unsigned int borderpx        = 2;              /* border pixel of windows */
static unsigned int gappx           = 1;              /* pixel gap between clients */
static unsigned int snap            = 30;             /* snap pixel */
static unsigned int gappih          = 8;              /* horiz inner gap between windows */
static unsigned int gappiv          = 8;              /* vert inner gap between windows */
static unsigned int gappoh          = 12;             /* horiz outer gap between windows and screen edge */
static unsigned int gappov          = 12;             /* vert outer gap between windows and screen edge */
static int user_bh                  = 30;              /* 0 means that AutoLinux will calculate bar height, >= 1 means AutoLinux will user_bh as bar height */
static int swallowfloating          = 5;              /* 5 means swallow floating windows by default */
static int smartgaps                = 0;              /* 1 means no outer gap when there is only one window */
static int showbar                  = 1;              /* 0 means no bar */
static int topbar                   = 1;              /* 0 means bottom bar */
static int horizpadbar              = 10;             /* horizontal padding for statusbar */
static int vertpadbar               = 15;             /* vertical padding for statusbar */
static int vertpad                  = 10;             /* vertical padding of bar */
static int sidepad                  = 10;             /* horizontal padding of bar */


static char dmenufont[]             =       "Mononoki Nerd Font:size=16.2:antialias=true:autohint=true" ;
static char *fonts[]                = {     "Mononoki Nerd Font:size=16.2:antialias=true:autohint=true",
                                            "monospace:size=11", "fontawesome:size=12",
                                            "JoyPixels:pixelsize=10:antialias=true:autohint=true",
                                            "Mononoki Nerd Font:size=9:antialias=true:autohint=true",
                                            "Hack:size=10:antialias=true:autohint=true",
                                            "JoyPixels:size=10:antialias=true:autohint=true"
};


//----------------------------------------------------------- Colors -----------------------------------------------------------//

/* static char selfgcolor[]            = #1A2026; */
static char normbgcolor[]           = "#1A2026";
static char normbordercolor[]       = "#222222";
static char normfgcolor[]           = "#41fdfe";
static char selfgcolor[]            = "#00ff00";
static char selbordercolor[]        = "#c9c9c9";
static char selbgcolor[]            = "#1A2026";
static char timefgcolor[]           = "#1A2026";
static char timebordercolor[]       = "#ff2782";
static char timebgcolor[]           = "#ff2782";
static char activefgcolor[]         = "#efeff0";
static char activebordercolor[]     = "#ff2782";
static char activebgcolor[]         = "#0077aa";
static char inactivefgcolor[]       = "#ff2782";
static char inactivebordercolor[]   = "#ff2782";
static char inactivebgcolor[]       = "#efeff0";

static char *colors[][3] = {

           /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
       [SchemeTime]  = { timefgcolor, timebgcolor, timebordercolor },
       [SchemeActive]  = { activefgcolor, activebgcolor, activebordercolor },
       [SchemeInactive]  = { inactivefgcolor, inactivebgcolor, inactivebordercolor },

};

typedef struct {
	const char *name;
	const void *cmd;
} Sp;
const char *spcmd1[] = {TERMINAL, "-n", "spterm", "-g", "160x50", NULL };
const char *spcmd2[] = {TERMINAL, "-n", "spcalc", "-f", "monospace:size=16", "-g", "50x20", "-e", "bc", "-lq", NULL };
const char *spcmd3[] = {TERMINAL, "-n", "spala", "-g", "160x50", NULL };
const char *spcmd4[] = {TERMINAL, "-n", "spalat", "-g", "160x50", "-e", "bindStart", NULL };
const char *spcmd5[] = {TERMINAL, "-n", "spalac", "-g", "175x52", NULL };
const char *spcmd6[] = {BROWSER,  NULL };

static Sp scratchpads[] = {

	/* name          cmd  */
	{"spterm",      spcmd1},
	{"spcalc",      spcmd2},
	{"spala",       spcmd3},
	{"spalat",      spcmd4},
	{"spslsc",      spcmd5},
	{"spbrowser",   spcmd6},
};

//----------------------------------------------------------- Tagging ----------------------------------------------------------//

static const char *tags[] =   { " 1 ", " 2 ", " 3 ", " 4 ", "  ", " 6 ", " 7 ", " 8 ", " 9 " };
//static const char *tags[] =   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " };
//static const char *tags[] =   { " 1 ", " 2 ", " 3 ", " 4 ", "  ", " 6 ", " 7 ", " 8 ", " 9 " };
//static const char *tags[] =   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " };
// static const char *tags[] =  {  " ", " ", " ", " ", " ", " ", " ", " ", " "  };


static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	*/
	/* class    instance      title       	 tags mask    isfloating   isterminal  noswallow  monitor */
	{ "Gimp",     NULL,       NULL,       	    1 << 8,       0,           0,         0,        -1 },
	{ TERMCLASS,  NULL,       NULL,       	    0,            0,           1,         0,        -1 },
	{ NULL,       NULL,       "Event Tester",   0,            0,           0,         1,        -1 },
	{ NULL,      "spterm",    NULL,       	    SPTAG(0),     1,           1,         0,        -1 },
	{ NULL,      "spcalc",    NULL,       	    SPTAG(1),     1,           1,         0,        -1 },
	{ NULL,      "spala",     NULL,             SPTAG(2),     1,           1,         0,        -1 },
	{ NULL,      "spalat",    NULL,             SPTAG(3),     1,           1,         0,        -1 },
	{ NULL,      "spalac",    NULL,             SPTAG(4),     1,           1,         0,        -1 },
	{ NULL,      "spbrowser", NULL,             SPTAG(5),     1,           1,         0,        -1 },
};


//-------------------------------------------------------- Bar Allignment -------------------------------------------------------//

static const BarRule barrules[] = {
   	/* monitor  bar    alignment               widthfunc              drawfunc              clickfunc           name */
 /* { -1,       0,     BAR_ALIGN_LEFT,         width_tags,            draw_tags,            click_tags,         "tags"     }, */
  	{ -1,       0,     BAR_ALIGN_CENTER,       width_tags,            draw_tags,            click_tags,         "tags"     },
    { -1,       0,     BAR_ALIGN_LEFT_LEFT,    width_ltsymbol,        draw_ltsymbol,        click_ltsymbol,     "layout"   },
    //{ 0,        0,     BAR_ALIGN_RIGHT_RIGHT,  width_time,            draw_time,            click_status,       "time"     },
    { -1,        0,     BAR_ALIGN_RIGHT_RIGHT,  width_date,            draw_date,            click_date,       "date"     },
    { 0,        0,     BAR_ALIGN_LEFT,         width_wintitle,        draw_wintitle,        click_wintitle,     "wintitle" },
    { 1,        0,     BAR_ALIGN_RIGHT_RIGHT,  width_time,            draw_time,            click_time,       "time"     },
    { 0,        0,     BAR_ALIGN_RIGHT_RIGHT,  width_time,            draw_time,            clickk_time,       "time"     },
};


//----------------------------------------------------------- Layouts -----------------------------------------------------------//

static float mfact     = 0.55; /* factor of master area size [0.1..0.90] */
static int nmaster     = 1;    /* number of clients in master area */
static int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",	monocle },			/* Default: Master on left, slaves on right */
	{ "",	bstack },		/* Master on top, slaves on bottom */

/*	{ "[@]",	spiral },		 Fibonacci spiral */
/*	{ "[\\]",	dwindle },		 Decreasing in size right and leftward */

	{ "",	deck },			/* Master on left, slaves in monocle-like mode on right */
	{ "",	tile },		/* All windows on top of eachother */

	{ "",	centeredmaster },		/* Master in middle, slaves on sides */
	{ "",	centeredfloatingmaster },	/* Same but master floats */

	{ "",	NULL },			/* no layout function means floating behavior */
	{ "",		NULL },
};


//--------------------------------------------------------- Key Definitions -----------------------------------------------------//


#define TAGKEYS(KEY,         TAG) \
	{ MODKEY,                       KEY,               view,           {.ui = 1 << TAG} }, \
	{ ALTKEY,                       KEY,               tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,               toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,               tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,               toggletag,      {.ui = 1 << TAG} },
#define STACKKEYS(MOD,ACTION) \
	{ MOD,	XK_j,	        ACTION##stack,	{.i = INC(+1) } }, \
	{ MOD,	XK_k,	        ACTION##stack,	{.i = INC(-1) } }, \
	{ MOD,  XK_BackSpace, ACTION##stack, {.i = PREVSEL } }, \
  { MOD,  XK_comma,    ACTION##stack,  {.i = 0 } }, \
	{ MOD,  XK_Up,	    ACTION##stack,	{.i = INC(+1) } }, \
	{ MOD,  XK_Down,	ACTION##stack,	{.i = INC(-1) } }, \
	/* { MOD, XK_grave, ACTION##stack, {.i = PREVSEL } }, \ */
	/* { MOD, XK_a,     ACTION##stack, {.i = 1 } }, \ */
	/* { MOD, XK_z,     ACTION##stack, {.i = 2 } }, \ */
	/* { MOD, XK_x,     ACTION##stack, {.i = -1 } }, */

/* helper for spawning shell commands in the pre AutoLinux-5.0 fashion */

/* commands */
static const char *termcmd[]   = { TERMINAL,  NULL };
static const char *term2cmd[]   = { TERMINAL2,  NULL };



//------------------------------------------  Xresources preferences to load at startup  ----------------------------------------//

ResourcePref resources[] = {
		{ "color0",	         STRING,	&normbordercolor },
		{ "color8",	         STRING,	&selbordercolor },
		{ "color0",	         STRING,	&normbgcolor },
		{ "color4",	         STRING,	&normfgcolor },
		{ "color0",	         STRING,	&selfgcolor },
		{ "color4",	         STRING,	&selbgcolor },
		{ "borderpx",        INTEGER,   &borderpx },
		{ "snap",	         INTEGER,   &snap },
		{ "showbar",         INTEGER,   &showbar },
		{ "topbar",	         INTEGER,   &topbar },
		{ "nmaster",         INTEGER,   &nmaster },
		{ "resizehints",	 INTEGER,   &resizehints },
		{ "mfact",           FLOAT,	    &mfact },
		{ "gappih",          INTEGER,   &gappih },
		{ "gappiv",          INTEGER,   &gappiv },
		{ "gappoh",          INTEGER,   &gappoh },
		{ "gappov",          INTEGER,   &gappov },
		{ "swallowfloating", INTEGER,   &swallowfloating },
		{ "smartgaps",		 INTEGER,   &smartgaps },
};

#include <X11/XF86keysym.h>
#include "shiftview.c"

//-------------------------------------------------------- Keybindings ----------------------------------------------------------//


static Key keys[] = {
	/* modifier                     key        function        argument */
	STACKKEYS(MODKEY,                          focus)
	STACKKEYS(ALTKEY,                          push)
	STACKKEYS(MODKEY|ShiftMask,                push)
	/* { MODKEY|ShiftMask,		XK_Escape,	spawn,	SHCMD("") }, */
	{ MODKEY,         			XK_grave,	spawn,	SHCMD("dmenuunicode") },
	/* { MODKEY|ShiftMask,		XK_grave,	togglescratch,	SHCMD("") }, */
	TAGKEYS(			XK_1,		0)
	TAGKEYS(			XK_2,		1)
	TAGKEYS(			XK_3,		2)
	TAGKEYS(			XK_4,		3)
	TAGKEYS(			XK_5,		4)
	TAGKEYS(			XK_6,		5)
	TAGKEYS(			XK_7,		6)
	TAGKEYS(			XK_8,		7)
	TAGKEYS(  		XK_9,		8)

  { MODKEY,             XK_F12,     spawn,		SHCMD("cpSecPass 1") },
  { MODKEY,             XK_F11,     spawn,		SHCMD("cpSecPass 2") },
  { MODKEY,             XK_F6,      spawn,		SHCMD("cpSecPass 3") },
  { MODKEY,             XK_F9,      spawn,		SHCMD("cpSecPass 5") },
  { MODKEY,             XK_F10,     spawn,		SHCMD("cpSecPass 6") },
  { MODKEY,             XK_F7,     spawn,		SHCMD("cpSecPass 8") },
  { ALTKEY|ShiftMask,   XK_F10,     spawn,		SHCMD("clipmenu") },
  { MODKEY,             XK_F11,     spawn,		SHCMD("clipmenu") },
  { MODKEY,             XK_F8,      spawn,		SHCMD("cpSecPass 7") },


  // Impossible Mapps (MODKEY + ALTKEY + ShiftMask + <key> :



  // Impossible Dirs:

  { IMPOSSIBLE,         XK_p,       spawn,     SHCMD("impossible_bindings p") },

  // Impossible Programs:

  { IMPOSSIBLE,         XK_p,       spawn,     SHCMD("impossible_bindings p") },
  { IMPOSSIBLE,         XK_w,       spawn,     SHCMD("impossible_bindings w") },
  { IMPOSSIBLE,         XK_e,       spawn,     SHCMD("impossible_bindings e") },
  { IMPOSSIBLE,         XK_r,       spawn,     SHCMD(TERMINAL2 " -e sh -c 'bpytop'") },
  { IMPOSSIBLE,         XK_t,       spawn,     SHCMD(TERMINAL2 " -e top") },
  { IMPOSSIBLE,         XK_a,       spawn,     SHCMD("impossible_bindings a") },
  { IMPOSSIBLE,         XK_s,       spawn,     SHCMD("impossible_bindings s") },
  { IMPOSSIBLE,         XK_S,       spawn,     SHCMD("impossible_bindings s") },
  { IMPOSSIBLE,         XK_W,       spawn,     SHCMD("impossible_bindings w") },
  { IMPOSSIBLE,         XK_d,       spawn,     SHCMD("impossible_bindings d") },
  { IMPOSSIBLE,         XK_f,       spawn,     SHCMD("impossible_bindings f") },
  { IMPOSSIBLE,         XK_g,       spawn,     SHCMD(TERMINAL2 " -e sh -c 'echo -en \"\\e[?25l\"; gpumon'") },
  { IMPOSSIBLE,         XK_z,       spawn,     SHCMD("impossible_bindings z") },
  { IMPOSSIBLE,         XK_x,       spawn,     SHCMD("impossible_bindings x") },
  { IMPOSSIBLE,         XK_c,       spawn,     SHCMD("impossible_bindings c") },
  { IMPOSSIBLE,         XK_v,       spawn,     SHCMD("impossible_bindings v") },
  { IMPOSSIBLE,         XK_b,       spawn,     SHCMD("impossible_bindings b") },
  { IMPOSSIBLE,         XK_period,  spawn,     SHCMD("impossible_bindings .") },

  { IMPOSSIBLE,         XK_y,       spawn,     SHCMD("impossible_bindings y") },
  { IMPOSSIBLE,         XK_u,       spawn,     SHCMD("impossible_bindings u") },
  { IMPOSSIBLE,         XK_i,       spawn,     SHCMD("impossible_bindings i") },
  { IMPOSSIBLE,         XK_o,       spawn,     SHCMD("impossible_bindings o") },
  { IMPOSSIBLE,         XK_p,       spawn,     SHCMD("impossible_bindings p") },
  { IMPOSSIBLE,         XK_h,       spawn,     SHCMD("impossible_bindings h") },
  { IMPOSSIBLE,         XK_j,       spawn,     SHCMD("impossible_bindings j") },
  { IMPOSSIBLE,         XK_k,       spawn,     SHCMD("impossible_bindings k") },
  { IMPOSSIBLE,         XK_l,       spawn,     SHCMD("impossible_bindings l") },
  { IMPOSSIBLE,         XK_n,       spawn,     SHCMD("impossible_bindings n") },
  { IMPOSSIBLE,         XK_m,       spawn,     SHCMD("impossible_bindings m") },

  { IMPOSSIBLE,         XK_0,       spawn,     SHCMD("impossible_bindings 0") },
  { IMPOSSIBLE,         XK_1,       spawn,     SHCMD("impossible_bindings 1") },
  { IMPOSSIBLE,         XK_2,       spawn,     SHCMD("impossible_bindings 2") },
  { IMPOSSIBLE,         XK_3,       spawn,     SHCMD("impossible_bindings 3") },
  { IMPOSSIBLE,         XK_4,       spawn,     SHCMD("impossible_bindings 4") },
  { IMPOSSIBLE,         XK_5,       spawn,     SHCMD("impossible_bindings 5") },
  { IMPOSSIBLE,         XK_6,       spawn,     SHCMD("impossible_bindings 6") },
  { IMPOSSIBLE,         XK_7,       spawn,     SHCMD("impossible_bindings 7") },
  { IMPOSSIBLE,         XK_8,       spawn,     SHCMD("impossible_bindings 8") },
  { IMPOSSIBLE,         XK_9,       spawn,     SHCMD("impossible_bindings 9") },

  { HARD,               XK_p,       spawn,     SHCMD("hard_bindings p") },
  { HARD,               XK_w,       spawn,     SHCMD("hard_bindings w") },
  { HARD,               XK_e,       spawn,     SHCMD("hard_bindings e") },
  { HARD,               XK_r,       spawn,     SHCMD("hard_bindings r") },
  { HARD,               XK_t,       spawn,     SHCMD("hard_bindings t") },
  { HARD,               XK_a,       spawn,     SHCMD("hard_bindings a") },
  { HARD,               XK_s,       spawn,     SHCMD("hard_bindings s") },
  { HARD,               XK_d,       spawn,     SHCMD("hard_bindings d") },
  { HARD,               XK_f,       spawn,     SHCMD("hard_bindings f") },
  { HARD,               XK_g,       spawn,     SHCMD("hard_bindings g") },
  { HARD,               XK_z,       spawn,     SHCMD("hard_bindings z") },
  { HARD,               XK_x,       spawn,     SHCMD("hard_bindings x") },
  { HARD,               XK_c,       spawn,     SHCMD("hard_bindings c") },
  { HARD,               XK_v,       spawn,     SHCMD("hard_bindings v") },
  { HARD,               XK_b,       spawn,     SHCMD("hard_bindings b") },
  { HARD,               XK_y,       spawn,     SHCMD("hard_bindings y") },
  { HARD,               XK_u,       spawn,     SHCMD("hard_bindings u") },
  { HARD,               XK_i,       spawn,     SHCMD("hard_bindings i") },
  { HARD,               XK_o,       spawn,     SHCMD("hard_bindings o") },
  { HARD,               XK_p,       spawn,     SHCMD("hard_bindings p") },
  { HARD,               XK_h,       spawn,     SHCMD("hard_bindings h") },
  { HARD,               XK_j,       spawn,     SHCMD("hard_bindings j") },
  { HARD,               XK_k,       spawn,     SHCMD("hard_bindings k") },
  { HARD,               XK_l,       spawn,     SHCMD("hard_bindings l") },
  { HARD,               XK_n,       spawn,     SHCMD("hard_bindings n") },
  { HARD,               XK_m,       spawn,     SHCMD("hard_bindings m") },


  { MODKEY,             XK_plus,    spawn,     SHCMD("hard_bindings +") },
  { MODKEY,             XK_equal,   spawn,     SHCMD("hard_bindings +") },
  { MODKEY,             XK_minus,   spawn,     SHCMD("hard_bindings -") },
  { MODKEY,             XK_F12,     spawn,     SHCMD("hard_bindings [") },




  { HARD,               XK_t,       spawn,     SHCMD(TERMINAL2 " -e sh -c 'td'") },



  { FUZZY,         XK_p,       spawn,     SHCMD("fuzzy_bindings p") },
  { FUZZY,         XK_1,       spawn,     SHCMD("fuzzy_bindings 1") },















  // Scratch Pads:

	{ MODKEY,		XK_s,	 togglescratch, {.ui = 0 } },
	{ ALTKEY,		XK_Return,  togglescratch, {.ui = 1 } },
	{ MODKEY,		XK_backslash,	togglescratch, {.ui = 4 } },
	{ MODKEY|ShiftMask,		XK_backslash,	togglescratch, {.ui = 3 } },
	{ MODKEY|ControlMask, XK_period,  spawn, SHCMD(TERMINAL " -e job") },
  { MODKEY,   XK_Escape,    spawn, SHCMD("startJustine") },
	{ MODKEY,   XK_slash,	    spawn, SHCMD("startJustineSilent") },




  { MODKEY,         			XK_0,		view,		{.ui = ~0 } },
	{ MODKEY|ShiftMask,		XK_0,		tag,		{.ui = ~0 } },
	{ MODKEY,         			XK_minus,	spawn,		SHCMD("pamixer --allow-boost -d 5; kill -44 $(pidof dwmblocks)") },
//	{ MODKEY|ShiftMask,		XK_minus,	spawn,		SHCMD("pamixer --allow-boost -d 15; kill -44 $(pidof dwmblocks)") },
 // { MODKEY,         			XK_equal,	spawn,		SHCMD("pamixer --allow-boost -i 5; kill -44 $(pidof dwmblocks)") },
	{ MODKEY|ShiftMask,		XK_equal,	spawn,		SHCMD("pamixer --allow-boost -i 15; kill -44 $(pidof dwmblocks)") },
	{ MODKEY,         			XK_BackSpace,	spawn,		SHCMD("sysact") },
	{ MODKEY|ShiftMask,		XK_BackSpace,	spawn,		SHCMD("sysact") },


/*  { MODKEY|ShiftMask,        XK_a, scratchpad_show, {0} },
	{ MODKEY|ShiftMask,        XK_s, scratchpad_hide, {0} },
	{ MODKEY|ShiftMask|ALTKEY, XK_equal,scratchpad_remove,{0} },
  */
	{ MODKEY,         			XK_Tab,		view,		{0} },
	{ ALTKEY,         			XK_Tab,	  spawn,		SHCMD("nemo") },
/* { MODKEY|ShiftMask,		XK_Tab,		spawn,		SHCMD("") }, */
	{ MODKEY,         			XK_q,		killclient,	{0} },
	{ MODKEY|ShiftMask,		XK_q,		spawn,		SHCMD("sysact") },
	{ MODKEY,         			XK_w,		spawn,		SHCMD("$BROWSER") },
	{ MODKEY|ShiftMask,	XK_w,		spawn,		SHCMD("$BROWSER2") },
  { MODKEY,                	XK_c,		spawn,		SHCMD("clipdl") },
  { MODKEY|ShiftMask,       XK_c,		spawn,		SHCMD(TERMINAL2 " -e tdap") },
  { CTRLKEY|ShiftMask,     XK_c,		spawn,		SHCMD("xdotool type 'connor@concise.cc'") },

/*	{ MODKEYask,		XK_w,		spawn,		SHCMD(TERMINAL " -e sudo nmtui") }, */
	{ MODKEY,         		XK_e,		spawn,		SHCMD(TERMINAL " -e neomutt ; pkill -RTMIN+12 dwmblocks; rmdir ~/.abook") },
	{ MODKEY|ShiftMask,		XK_e,		spawn,		SHCMD("rofi -modi emoji -show emoji") },
	{ MODKEY,         		XK_r,		spawn,		SHCMD(TERMINAL2 " -e sh -c 'bpytop'") },
	{ MODKEY|ShiftMask,		XK_r,		spawn,		SHCMD(TERMINAL2 " -e sh -c 'bpytop'") },
	{ ALTKEY,         		XK_y,	  spawn,     SHCMD("youtube-music") },
	{ MODKEY,         		XK_t,		setlayout,	{.v = &layouts[0]} }, /* tile */
	{ MODKEY,		          XK_i,		setlayout,	{.v = &layouts[3]} }, /* dwindle */
	{ MODKEY,         		XK_u,		setlayout,	{.v = &layouts[2]} }, /* deck */
	{ MODKEY,         		XK_y,		setlayout,	{.v = &layouts[0]} }, /* centeredmaster */
	{ MODKEY,         		XK_o,		setlayout,  {.v = &layouts[4]} },
	{ MODKEY|ShiftMask,		XK_u,		setlayout,	{.v = &layouts[5]} }, /* monocle */
	{ MODKEY|ShiftMask,		XK_i,		setlayout,	{.v = &layouts[7]} }, /* centeredfloatingmaster */
	{ MODKEY|ShiftMask,		XK_t,		setlayout,	{.v = &layouts[1]} }, /* bstack */
	{ MODKEY|ShiftMask,   XK_y,		setlayout,	{.v = &layouts[4]} }, /* spiral */
	{ MODKEY,         		XK_p,		incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,		XK_p,		incnmaster,     {.i = -1 } },
/*	{ MODKEY,         			XK_p,		spawn,		SHCMD("mpc toggle") },
	{ MODKEY|ShiftMask,	XK_p,		spawn,		SHCMD("mpc pause ; pauseallmpv") },*/

    { MODKEY,         			XK_bracketleft,		spawn,		SHCMD("mpc seek -10") },
	{ MODKEY|ShiftMask,	XK_bracketleft,		spawn,		SHCMD("mpc seek -60") },
	{ MODKEY,         			XK_bracketright,	spawn,		SHCMD("mpc seek +10") },
	{ MODKEY|ShiftMask,	XK_bracketright,	spawn,		SHCMD("mpc seek +60") },
//	{ MODKEY,         			XK_backslash,		view,		{0} },
	/* { MODKEY|ShiftMask,		XK_backslash,		spawn,		SHCMD("") }, */
//  { ALTKEY,             XK_backslash,	spawn,		SHCMD(TERMINAL2 " -e scratchFile") },
  { CTRLKEY|ShiftMask,   XK_equal,	spawn,		SHCMD("aopac +") },
  { CTRLKEY|ShiftMask,   XK_minus,	spawn,		SHCMD("aopac -") },


	{ MODKEY,         			XK_a,		togglegaps,	{0} },
	{ MODKEY|ShiftMask,     XK_a,		defaultgaps,	{0} },
//	{ MODKEY,         			XK_s,		togglesticky,	{0} },
  { MODKEY|ShiftMask,		XK_s,		spawn,     SHCMD("gnome-screenshot -i") },
	{ MODKEY,         			XK_d,		spawn,          SHCMD("rfi") },
	{ MODKEY|ALTKEY,         			XK_d,		spawn,  SHCMD("rfi") },
	{ MODKEY|ALTKEY,         		  XK_b,		spawn,		SHCMD("passmenu") },
	{ MODKEY,               XK_f,		togglefullscr,	{0} },
	{ MODKEY|ShiftMask,     XK_f,		setlayout,	{.v = &layouts[8]} },
	{ MODKEY,               XK_g,		shiftview,	{ .i = -1 } },
	{ MODKEY|ShiftMask,     XK_g,		shifttag,	{ .i = -1 } },
  { ALTKEY,               XK_g,		spawn,		SHCMD(TERMINAL2 " -e gpumon") },
	{ MODKEY,               XK_h,		setmfact,	{.f = -0.05} },
	{ ALTKEY,               XK_h,		spawn,    SHCMD(TERMINAL2 " -e top") },
	{ MODKEY,               XK_l,		setmfact,      	{.f = +0.05} },
	{ MODKEY|ShiftMask,     XK_l,		spawn,    SHCMD("formdata") },
  { CTRLKEY|ShiftMask,    XK_l,		spawn,    SHCMD("chlnk") },
//	{ MODKEY,               XK_semicolon,	shiftview,	{ .i = 1 } },
	{ MODKEY|ShiftMask,     XK_semicolon,	shifttag,	{ .i = 1 } },
	{ ALTKEY|ShiftMask,     XK_p, spawn, SHCMD(TERMINAL2 " -e snipSnip&&notify-send '✅ Coppied from Clipboard'") },
	{ ALTKEY|ShiftMask,     XK_o, spawn, SHCMD(TERMINAL2 " -e snipSnip -o&&notify-send '✅ Saved to Clipboard'") },
	{ MODKEY,               XK_apostrophe,	togglescratch,	{.ui = 1} },
	/* { MODKEY|ShiftMask,	XK_apostrophe,	spawn,		SHCMD("") }, */
	{ MODKEY,               XK_Return,	spawn,		{.v = term2cmd } },
	{ MODKEY,               XK_z,		incrgaps,	{.i = +3 } },
	/* { MODKEY|ShiftMask,	XK_z,		spawn,		SHCMD("") }, */
	{ MODKEY,               XK_x,		incrgaps,	{.i = -3 } },
	/* { MODKEY|ShiftMask,	XK_x,		spawn,		SHCMD("") }, */
	/* { MODKEY,         		XK_c,		spawn,		SHCMD("") }, */
	/* { MODKEY|ShiftMask,	XK_c,		spawn,		SHCMD("") }, */
	/* V is automatically bound above in STACKKEYS */
	{ MODKEY,         			XK_b,		togglebar,	{0} },
	{ MODKEY|ShiftMask,			XK_b,		spawn,    SHCMD("blueberry") },
	/* { MODKEY|ShiftMask, 	XK_b,		spawn,		SHCMD("") }, */
	{ MODKEY,         			XK_n,		spawn,		SHCMD(TERMINAL2 " -e vnote") },
	{ MODKEY|ShiftMask,     XK_n,		spawn,		SHCMD(TERMINAL2 " -e [ ! -d $HOME/Documents ] && mkdir $HOME/Documents >/dev/null 2>&1; ${TERMINAL} -e nvim $HOME/Documents/notes") },
  { ALTKEY,               XK_n,		spawn,		SHCMD(TERMINAL2 " -e nemo") },
  { ALTKEY,               XK_m,		spawn,		SHCMD(TERMINAL2 " -e top") },
	{ MODKEY,         			XK_m,		spawn,		SHCMD(TERMINAL2 " -e ncmpcpp") },

	{ MODKEY|ShiftMask,     XK_m,		spawn,		SHCMD(TERMINAL " -e mpv `xclip -selection primary -o`") },
	{ MODKEY,         			XK_comma,	spawn,		SHCMD("mpc prev") },
	{ MODKEY|ShiftMask,     XK_comma,	spawn,		SHCMD("mpc seek 0%") },
	{ MODKEY,         			XK_period,	spawn,		SHCMD(TERMINAL2 " -e qcopy") },
	{ ALTKEY,         			XK_period,	spawn,		SHCMD(TERMINAL2 " -e editenv --ignore-sourced-warning") },
	{ MODKEY|ShiftMask,     XK_period,	spawn,		SHCMD("mpc repeat") },

	{ MODKEY,         			XK_Left,	focusmon,	{.i = -1 } },
	{ MODKEY|ShiftMask,     XK_Left,	tagmon,		{.i = -1 } },
	{ MODKEY,         			XK_Right,	focusmon,	{.i = +1 } },
	{ MODKEY|ShiftMask,     XK_Right,	tagmon,		{.i = +1 } },

	{ MODKEY,         			XK_Page_Up,	shiftview,	{ .i = -1 } },
	{ MODKEY|ShiftMask,     XK_Page_Up,	shifttag,	{ .i = -1 } },
	{ MODKEY,         			XK_Page_Down,	shiftview,	{ .i = +1 } },
	{ MODKEY|ShiftMask,     XK_Page_Down,	shifttag,	{ .i = +1 } },
	{ MODKEY,         			XK_Insert,	spawn,		SHCMD("xdotool type $(grep -v '^#' ~/.local/share/Nu1LL1nuX/snippets | dmenu -i -l 50 | cut -d' ' -f1)") },

	{ MODKEY,         			XK_F2,		spawn,		SHCMD("flameshot gui") },
	{ MODKEY,         			XK_F1,		spawn,		SHCMD("qocr") },
	{ MODKEY,         			XK_F6,		spawn,		SHCMD("qocre") },
	{ MODKEY,         			XK_F3,		spawn,		SHCMD("displayselect") },
	{ MODKEY,         			XK_F4,		spawn,		SHCMD(TERMINAL2 " -e pulsemixer; kill -44 $(pidof dwmblocks)") },
  { MODKEY,         		  XK_F5,		spawn,		SHCMD("pavucontrol") },
//	{ MODKEY,         			XK_F6,		spawn,		SHCMD("torwrap") },
//	{ MODKEY,         			XK_F7,		spawn,		SHCMD("td-toggle") },
//	{ MODKEY,         			XK_F8,		spawn,		SHCMD("mw -Y") },
//	{ MODKEY,         			XK_F9,		spawn,		SHCMD("/usr/bin/st -e 'vfzf $custDir'") },
//	{ MODKEY,         			XK_F10,		spawn,		SHCMD(TERMINAL " -e [ ! -d ${custDir} ] && mkdir ${custDir} >/dev/null 2>&1; ${TERMINAL} -e nvim ${custDir}/`find $HOME/CONCISE/repos -type f|sed 's/^.\//'|fzfp`)") },
//  { MODKEY,         			XK_F11,		spawn,		SHCMD("mpv --no-cache --no-osc --no-input-default-bindings --profile=low-latency --input-conf=/dev/null --title=webcam $(ls /dev/video[0,2,4,6,8] | tail -n 1)") },
	//{ MODKEY,         			XK_F12,		spawn,		SHCMD(TERMINAL " -e [ ! -d ${HOME}/SCRATCH ] && mkdir ${HOME}/SCRATCH >/dev/null 2>&1; ${TERMINAL} -e /usr/bin/lfp ${HOME}/SCRATCH") },
	{ MODKEY,                	XK_space,	spawn,	    SHCMD(TERMINAL2 " -e /usr/bin/snip") },
	{ MODKEY|ShiftMask,	XK_space,	zoom,	        {0} },
	{ MODKEY|ShiftMask,	XK_space,	togglefloating,	{0} },

	{ 0,				XK_Print,	spawn,		SHCMD("maim pic-full-$(date '+%y%m%d-%H%M-%S').png") },
	{ ShiftMask,			XK_Print,	spawn,		SHCMD("maimpick") },
	{ MODKEY,         			XK_Print,	spawn,		SHCMD("dmenurecord") },
	{ MODKEY|ShiftMask,		XK_Print,	spawn,		SHCMD("dmenurecord kill") },
	{ MODKEY,         			XK_Delete,	spawn,		SHCMD("dmenurecord kill") },
	{ MODKEY,         			XK_Scroll_Lock,	spawn,		SHCMD("killall screenkey || screenkey &") },

	{ 0, XF86XK_AudioMute,		spawn,		SHCMD("pamixer -t; kill -44 $(pidof dwmblocks)") },
	{ 0, XF86XK_AudioRaiseVolume,	spawn,		SHCMD("pamixer --allow-boost -i 3; kill -44 $(pidof dwmblocks)") },
	{ 0, XF86XK_AudioLowerVolume,	spawn,		SHCMD("pamixer --allow-boost -d 3; kill -44 $(pidof dwmblocks)") },
	{ 0, XF86XK_AudioPrev,		spawn,		SHCMD("mpc prev") },
	{ 0, XF86XK_AudioNext,		spawn,		SHCMD("mpc next") },
	{ 0, XF86XK_AudioPause,		spawn,		SHCMD("mpc pause") },
	{ 0, XF86XK_AudioPlay,		spawn,		SHCMD("mpc play") },
	{ 0, XF86XK_AudioStop,		spawn,		SHCMD("mpc stop") },
	{ 0, XF86XK_AudioRewind,	spawn,		SHCMD("mpc seek -10") },
	{ 0, XF86XK_AudioForward,	spawn,		SHCMD("mpc seek +10") },
	{ 0, XF86XK_AudioMedia,		spawn,		SHCMD(TERMINAL " -e ncmpcpp") },
	{ 0, XF86XK_AudioMicMute,	spawn,		SHCMD("pactl set-source-mute @DEFAULT_SOURCE@ toggle") },
	{ 0, XF86XK_PowerOff,		spawn,		SHCMD("sysact") },
	{ 0, XF86XK_Calculator,		spawn,		SHCMD(TERMINAL " -e bc -l") },
	{ 0, XF86XK_Sleep,		spawn,		SHCMD("sudo -A zzz") },
	{ 0, XF86XK_WWW,		spawn,		SHCMD("$BROWSER") },
	{ 0, XF86XK_DOS,		spawn,		SHCMD(TERMINAL) },
	{ 0, XF86XK_ScreenSaver,	spawn,		SHCMD("slock & xset dpms force off; mpc pause; pauseallmpv") },
	{ 0, XF86XK_TaskPane,		spawn,		SHCMD(TERMINAL " -e top") },
	{ 0, XF86XK_Mail,		spawn,		SHCMD(TERMINAL " -e neomutt ; pkill -RTMIN+12 dwmblocks") },
	{ 0, XF86XK_MyComputer,		spawn,		SHCMD(TERMINAL " -e lf /") },
	/* { 0, XF86XK_Battery,		spawn,		SHCMD("") }, */
	{ 0, XF86XK_Launch1,		spawn,		SHCMD("xset dpms force off") },
	{ 0, XF86XK_TouchpadToggle,	spawn,		SHCMD("(synclient | grep 'TouchpadOff.*1' && synclient TouchpadOff=0) || synclient TouchpadOff=1") },
	{ 0, XF86XK_TouchpadOff,	spawn,		SHCMD("synclient TouchpadOff=1") },
	{ 0, XF86XK_TouchpadOn,		spawn,		SHCMD("synclient TouchpadOff=0") },
	{ 0, XF86XK_MonBrightnessUp,	spawn,		SHCMD("xbacklight -inc 15") },
	{ 0, XF86XK_MonBrightnessDown,	spawn,		SHCMD("xbacklight -dec 15") },

	/* { MODKEY|Mod4Mask,              XK_h,      incrgaps,       {.i = +1 } }, */
	/* { MODKEY|Mod4Mask,              XK_l,      incrgaps,       {.i = -1 } }, */
	/* { MODKEY|Mod4Mask|ShiftMask,    XK_h,      incrogaps,      {.i = +1 } }, */
	/* { MODKEY|Mod4Mask|ShiftMask,    XK_l,      incrogaps,      {.i = -1 } }, */
	/* { MODKEY|Mod4Mask|ControlMask,  XK_h,      incrigaps,      {.i = +1 } }, */
	/* { MODKEY|Mod4Mask|ControlMask,  XK_l,      incrigaps,      {.i = -1 } }, */
	/* { MODKEY|Mod4Mask|ShiftMask,    XK_0,      defaultgaps,    {0} }, */
	/* { MODKEY,                                XK_y,      incrihgaps,     {.i = +1 } }, */
	/* { MODKEY,                                XK_o,      incrihgaps,     {.i = -1 } }, */
	/* { MODKEY|ControlMask,           XK_y,      incrivgaps,     {.i = +1 } }, */
	/* { MODKEY|ControlMask,           XK_o,      incrivgaps,     {.i = -1 } }, */
	/* { MODKEY|Mod4Mask,              XK_y,      incrohgaps,     {.i = +1 } }, */
	/* { MODKEY|Mod4Mask,              XK_o,      incrohgaps,     {.i = -1 } }, */
	/* { MODKEY|ShiftMask,             XK_y,      incrovgaps,     {.i = +1 } }, */
	/* { MODKEY|ShiftMask,             XK_o,      incrovgaps,     {.i = -1 } }, */

};


/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
#ifndef __OpenBSD__
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button1,        sigdwmblocks,   {.i = 1} },
	{ ClkStatusText,        0,              Button2,        sigdwmblocks,   {.i = 2} },
	{ ClkStatusText,        0,              Button3,        sigdwmblocks,   {.i = 3} },
	{ ClkStatusText,        0,              Button4,        sigdwmblocks,   {.i = 4} },
	{ ClkStatusText,        0,              Button5,        sigdwmblocks,   {.i = 5} },
	{ ClkStatusText,        ShiftMask,      Button1,        sigdwmblocks,   {.i = 6} },
#endif
	{ ClkStatusText,        ShiftMask,      Button3,        spawn,          SHCMD(TERMINAL " -e nvim ~/.local/src/dwmblocks/config.h") },
	{ ClkClientWin,         MODKEY,                  Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,                  Button2,        defaultgaps,	{0} },
	{ ClkClientWin,         MODKEY,                  Button3,        resizemouse,    {0} },
	{ ClkClientWin,		MODKEY,         		Button4,	incrgaps,	{.i = +1} },
	{ ClkClientWin,		MODKEY,         		Button5,	incrgaps,	{.i = -1} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,                  Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,                  Button3,        toggletag,      {0} },
	{ ClkTagBar,		0,		Button4,	shiftview,	{.i = -1} },
	{ ClkTagBar,		0,		Button5,	shiftview,	{.i = 1} },
	{ ClkRootWin,		0,		Button2,	togglebar,	{0} },
};
