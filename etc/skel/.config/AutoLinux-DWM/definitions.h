
#define TERMINAL "st"
#define TERMCLASS "St"
#define TERMINAL2 "alacritty"
#define TERMCLASS2 "Alacritty"
#define BROWSER "brave"
#define MODKEY Mod4Mask
#define ALTKEY Mod1Mask
#define CTRLKEY ControlMask
#define NOMOD 0

#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

