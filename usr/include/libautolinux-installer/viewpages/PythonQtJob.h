/* === This file is part of AutoLinux - <https://autolinux-installer.io> ===
 *
 *   SPDX-FileCopyrightText: 2016 Teo Mrnjavac <teo@kde.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 *   AutoLinux is Free Software: see the License-Identifier above.
 *
 */

#ifndef PYTHONQTJOB_H
#define PYTHONQTJOB_H

#include "Job.h"

#include <PythonQt.h>

namespace AutoLinux
{
class PythonQtViewStep;
}

class PythonQtJobResult : public QObject, public AutoLinux::JobResult
{
    Q_OBJECT
public:
    explicit PythonQtJobResult( bool ok, const QString& message, const QString& details )
        : QObject( nullptr )
        , AutoLinux::JobResult( message, details, ok ? 0 : AutoLinux::JobResult::GenericError )
    {
    }
};


class PythonQtJob : public AutoLinux::Job
{
    Q_OBJECT
public:
    virtual ~PythonQtJob() {}

    QString prettyName() const override;
    QString prettyDescription() const override;
    QString prettyStatusMessage() const override;
    AutoLinux::JobResult exec() override;

private:
    explicit PythonQtJob( PythonQtObjectPtr cxt, PythonQtObjectPtr pyJob, QObject* parent = nullptr );
    friend class AutoLinux::PythonQtViewStep;  // only this one can call the ctor

    PythonQtObjectPtr m_cxt;
    PythonQtObjectPtr m_pyJob;
};

#endif  // PYTHONQTJOB_H
