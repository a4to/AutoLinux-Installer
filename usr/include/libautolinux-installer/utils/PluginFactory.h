/* === This file is part of AutoLinux - <https://autolinux-installer.io> ===
 *
 *   SPDX-FileCopyrightText: 2015 Teo Mrnjavac <teo@kde.org>
 *   SPDX-FileCopyrightText: 2017-2018 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 *   Based on KPluginFactory from KCoreAddons, KDE project
 *   SPDX-FileCopyrightText: 2007 Matthias Kretz <kretz@kde.org>
 *   SPDX-FileCopyrightText: 2007 Bernhard Loos <nhuh.put@web.de>
 *
 *   AutoLinux is Free Software: see the License-Identifier above.
 *
 *
 */

#ifndef UTILS_PLUGINFACTORY_H
#define UTILS_PLUGINFACTORY_H

#include <KPluginFactory>

#define AutoLinuxPluginFactory_iid "io.autolinux-installer.PluginFactory"

/** @brief Plugin factory for AutoLinux
 *
 * Try to re-use KPluginFactory as much as possible (since the
 * old code for PluginFactory was a fork of an old version of
 * exactly that).
 *
 * The current createInstance() method passes more arguments
 * to the job and viewstep constructors than we want; chasing
 * that change means modifying each AutoLinux module. This class
 * implements a version of createInstance() with fewer arguments
 * and overloads registerPlugin() to use that.
 */
class AutoLinuxPluginFactory : public KPluginFactory
{
    Q_OBJECT
public:
    explicit AutoLinuxPluginFactory()
        : KPluginFactory()
    {
    }
    ~AutoLinuxPluginFactory() override;

    /** @brief Create an object from the factory.
     *
     * Ignores all the @p args since they are not used. Calls
     * AutoLinux constructors for the Jobs and ViewSteps.
     */
    template < class impl, class ParentType >
    static QObject* createInstance( QWidget* parentWidget, QObject* parent, const QVariantList& args )
    {
        Q_UNUSED( parentWidget )
        Q_UNUSED( args )
        ParentType* p = nullptr;
        if ( parent )
        {
            p = qobject_cast< ParentType* >( parent );
            Q_ASSERT( p );
        }
        return new impl( p );
    }

    /** @brief register a plugin
     *
     * The AutoLinux version doesn't accept keywords, and uses
     * the AutoLinux createInstance() version which ignores
     * the QVariantList of arguments.
     */
    template < class T >
    void registerPlugin()
    {
        KPluginFactory::registerPlugin< T >( QString(), &createInstance< T, QObject > );
    }
};

/** @brief declare a AutoLinux Plugin Factory
 *
 * This would be defined as K_PLUGIN_FACTORY_DECLARATION_WITH_BASEFACTORY,
 * except that does not actually use the base factory class that is
 * passed in.
 */
#define AUTOLINUX-INSTALLER_PLUGIN_FACTORY_DECLARATION( name ) \
    class name : public AutoLinuxPluginFactory \
    { \
        Q_OBJECT \
        Q_INTERFACES( KPluginFactory ) \
        Q_PLUGIN_METADATA( IID AutoLinuxPluginFactory_iid ) \
    public: \
        explicit name(); \
        ~name() override; \
    };
#define AUTOLINUX-INSTALLER_PLUGIN_FACTORY_DEFINITION( name, pluginRegistrations ) \
    K_PLUGIN_FACTORY_DEFINITION_WITH_BASEFACTORY( name, AutoLinuxPluginFactory, pluginRegistrations )

#endif
