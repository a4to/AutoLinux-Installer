/* === This file is part of AutoLinux - <https://autolinux-installer.io> ===
 *
 *   SPDX-FileCopyrightText: 2012 Christian Muehlhaeuser <muesli@tomahawk-player.org>
 *   SPDX-FileCopyrightText: 2019 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

#ifndef IMAGE_REGISTRY_H
#define IMAGE_REGISTRY_H

#include <QPixmap>

#include "DllMacro.h"
#include "utils/AutoLinuxUtilsGui.h"

class UIDLLEXPORT ImageRegistry
{
public:
    static ImageRegistry* instance();

    explicit ImageRegistry();

    QIcon icon( const QString& image, AutoLinuxUtils::ImageMode mode = AutoLinuxUtils::Original );
    QPixmap
    pixmap( const QString& image, const QSize& size, AutoLinuxUtils::ImageMode mode = AutoLinuxUtils::Original );

private:
    qint64 cacheKey( const QSize& size );
    void putInCache( const QString& image, const QSize& size, AutoLinuxUtils::ImageMode mode, const QPixmap& pixmap );
};

#endif  // IMAGE_REGISTRY_H
