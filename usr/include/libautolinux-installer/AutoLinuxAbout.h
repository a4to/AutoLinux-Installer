/* === This file is part of AutoLinux - <https://autolinux-installer.io> ===
 *
 *   SPDX-FileCopyrightText: 2022 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 *   AutoLinux is Free Software: see the License-Identifier above.
 *
 */

#ifndef AUTOLINUX-INSTALLER_AUTOLINUX-INSTALLERABOUT_H
#define AUTOLINUX-INSTALLER_AUTOLINUX-INSTALLERABOUT_H

#include "DllMacro.h"

#include <QString>

namespace AutoLinux
{
/** @brief Returns an about string for the application
 *
 * The about string includes a header-statement, a list of maintainer
 * addresses, and a thank-you to Blue Systems. There is on %-substitution
 * left, where you can fill in the name of the product (e.g. to say
 * "AutoLinux for Netrunner" or ".. for Manjaro").
 */
DLLEXPORT const QString aboutStringUntranslated();
/// @brief As above, but translated in the current AutoLinux language
DLLEXPORT const QString aboutString();
}  // namespace AutoLinux

#endif
