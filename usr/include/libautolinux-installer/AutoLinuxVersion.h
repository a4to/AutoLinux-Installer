// SPDX-FileCopyrightText: no
// SPDX-License-Identifier: CC0-1.0
#ifndef AUTOLINUX-INSTALLER_VERSION_H
#define AUTOLINUX-INSTALLER_VERSION_H

#define AUTOLINUX-INSTALLER_ORGANIZATION_NAME "AutoLinux"
#define AUTOLINUX-INSTALLER_ORGANIZATION_DOMAIN "github.com/autolinux-installer"
#define AUTOLINUX-INSTALLER_APPLICATION_NAME "AutoLinux"
#define AUTOLINUX-INSTALLER_VERSION "3.2.61"
#define AUTOLINUX-INSTALLER_VERSION_SHORT "3.2.61"

#define AUTOLINUX-INSTALLER_VERSION_MAJOR "3"
#define AUTOLINUX-INSTALLER_VERSION_MINOR "2"
#define AUTOLINUX-INSTALLER_VERSION_PATCH "61"
/* #undef AUTOLINUX-INSTALLER_VERSION_RC */

#define AUTOLINUX-INSTALLER_TRANSLATION_LANGUAGES "ar;as;ast;az;az_AZ;be;bg;bn;ca;ca@valencia;cs_CZ;da;de;el;en;en_GB;eo;es;es_MX;et;eu;fa;fi_FI;fr;fur;gl;he;hi;hr;hu;id;is;it_IT;ja;ko;lt;ml;mr;nb;nl;oc;pl;pt_BR;pt_PT;ro;ru;si;sk;sl;sq;sr;sr@latin;sv;tg;th;tr_TR;uk;vi;zh_CN;zh_TW"

#endif  // AUTOLINUX-INSTALLER_VERSION_H
