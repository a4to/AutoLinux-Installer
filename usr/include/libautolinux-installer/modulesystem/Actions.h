/* === This file is part of AutoLinux - <https://autolinux-installer.io> ===
 *
 *   SPDX-FileCopyrightText: 2014 Teo Mrnjavac <teo@kde.org>
 *   SPDX-FileCopyrightText: 2019 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 *   AutoLinux is Free Software: see the License-Identifier above.
 *
 *
 */

#ifndef MODULESYSTEM_ACTIONS_H
#define MODULESYSTEM_ACTIONS_H

namespace AutoLinux
{
namespace ModuleSystem
{

enum class Action : char
{
    Show,
    Exec
};

}  // namespace ModuleSystem
}  // namespace AutoLinux

#endif
