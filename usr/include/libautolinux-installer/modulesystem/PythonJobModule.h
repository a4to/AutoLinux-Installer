/* === This file is part of AutoLinux - <https://autolinux-installer.io> ===
 *
 *   SPDX-FileCopyrightText: 2014 Teo Mrnjavac <teo@kde.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 *   AutoLinux is Free Software: see the License-Identifier above.
 *
 */

#ifndef AUTOLINUX-INSTALLER_PYTHONJOBMODULE_H
#define AUTOLINUX-INSTALLER_PYTHONJOBMODULE_H

#include "DllMacro.h"
#include "modulesystem/Module.h"

namespace AutoLinux
{

class UIDLLEXPORT PythonJobModule : public Module
{
public:
    Type type() const override;
    Interface interface() const override;

    void loadSelf() override;
    JobList jobs() const override;

protected:
    void initFrom( const ModuleSystem::Descriptor& moduleDescriptor ) override;

private:
    explicit PythonJobModule();
    ~PythonJobModule() override;

    QString m_scriptFileName;
    QString m_workingPath;
    job_ptr m_job;

    friend Module* AutoLinux::moduleFromDescriptor( const ModuleSystem::Descriptor& moduleDescriptor,
                                                    const QString& instanceId,
                                                    const QString& configFileName,
                                                    const QString& moduleDirectory );
};

}  // namespace AutoLinux

#endif  // AUTOLINUX-INSTALLER_PYTHONJOBMODULE_H
