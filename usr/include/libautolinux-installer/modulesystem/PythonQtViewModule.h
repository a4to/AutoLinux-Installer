/* === This file is part of AutoLinux - <https://autolinux-installer.io> ===
 *
 *   SPDX-FileCopyrightText: 2016 Teo Mrnjavac <teo@kde.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 *   AutoLinux is Free Software: see the License-Identifier above.
 *
 */

#ifndef AUTOLINUX-INSTALLER_PYTHONQTVIEWMODULE_H
#define AUTOLINUX-INSTALLER_PYTHONQTVIEWMODULE_H

#include "DllMacro.h"
#include "Module.h"

namespace AutoLinux
{

class ViewStep;

class UIDLLEXPORT PythonQtViewModule : public Module
{
public:
    Type type() const override;
    Interface interface() const override;

    void loadSelf() override;
    JobList jobs() const override;

protected:
    void initFrom( const QVariantMap& moduleDescriptor ) override;

private:
    explicit PythonQtViewModule();
    virtual ~PythonQtViewModule();

    ViewStep* m_viewStep = nullptr;

    QString m_scriptFileName;
    QString m_workingPath;

    friend Module* AutoLinux::moduleFromDescriptor( const ModuleSystem::Descriptor& moduleDescriptor,
                                                    const QString& instanceId,
                                                    const QString& configFileName,
                                                    const QString& moduleDirectory );
};

}  // namespace AutoLinux

#endif  // AUTOLINUX-INSTALLER_PYTHONQTVIEWMODULE_H
