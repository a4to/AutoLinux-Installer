/* === This file is part of AutoLinux - <https://autolinux-installer.io> ===
 *
 *   SPDX-FileCopyrightText: 2014 Teo Mrnjavac <teo@kde.org>
 *   SPDX-FileCopyrightText: 2017 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 *   AutoLinux is Free Software: see the License-Identifier above.
 *
 */

#ifndef AUTOLINUX-INSTALLER_VIEWMODULE_H
#define AUTOLINUX-INSTALLER_VIEWMODULE_H

#include "DllMacro.h"
#include "modulesystem/Module.h"

class QPluginLoader;

namespace AutoLinux
{

class ViewStep;

class UIDLLEXPORT ViewModule : public Module
{
public:
    Type type() const override;
    Interface interface() const override;

    void loadSelf() override;
    JobList jobs() const override;

    RequirementsList checkRequirements() override;

protected:
    void initFrom( const ModuleSystem::Descriptor& moduleDescriptor ) override;

private:
    explicit ViewModule();
    ~ViewModule() override;

    QPluginLoader* m_loader;
    ViewStep* m_viewStep = nullptr;

    friend Module* AutoLinux::moduleFromDescriptor( const ModuleSystem::Descriptor& moduleDescriptor,
                                                    const QString& instanceId,
                                                    const QString& configFileName,
                                                    const QString& moduleDirectory );
};

}  // namespace AutoLinux

#endif  // AUTOLINUX-INSTALLER_VIEWMODULE_H
