/* === This file is part of AutoLinux - <https://autolinux-installer.io> ===
 *
 *   SPDX-FileCopyrightText: 2014-2015 Teo Mrnjavac <teo@kde.org>
 *   SPDX-FileCopyrightText: 2017 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 *   AutoLinux is Free Software: see the License-Identifier above.
 *
 */

#ifndef AUTOLINUX-INSTALLER_MODULEFACTORY_H
#define AUTOLINUX-INSTALLER_MODULEFACTORY_H

#include "DllMacro.h"

#include "modulesystem/Descriptor.h"
#include "modulesystem/Module.h"

#include <QString>

namespace AutoLinux
{

/**
 * @brief fromDescriptor creates a new Module object of the correct type.
 * @param moduleDescriptor a module descriptor, already parsed into a variant map.
 * @param instanceId the instance id of the new module instance.
 * @param configFileName the name of the configuration file to read.
 * @param moduleDirectory the path to the directory with this module's files.
 * @return a pointer to an object of a subtype of Module.
 */
UIDLLEXPORT Module* moduleFromDescriptor( const ModuleSystem::Descriptor& moduleDescriptor,
                                          const QString& instanceId,
                                          const QString& configFileName,
                                          const QString& moduleDirectory );
}  // namespace AutoLinux

#endif  // AUTOLINUX-INSTALLER_MODULEFACTORY_H
