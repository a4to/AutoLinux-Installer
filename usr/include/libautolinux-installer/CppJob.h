/* === This file is part of AutoLinux - <https://autolinux-installer.io> ===
 *
 *   SPDX-FileCopyrightText: 2014-2015 Teo Mrnjavac <teo@kde.org>
 *   SPDX-FileCopyrightText: 2016 Kevin Kofler <kevin.kofler@chello.at>
 *   SPDX-FileCopyrightText: 2020 Adriaan de Groot <groot@kde.org>
 *   SPDX-License-Identifier: GPL-3.0-or-later
 *
 *   AutoLinux is Free Software: see the License-Identifier above.
 *
 */

#ifndef AUTOLINUX-INSTALLER_CPPJOB_H
#define AUTOLINUX-INSTALLER_CPPJOB_H

#include "DllMacro.h"
#include "Job.h"

#include "modulesystem/InstanceKey.h"

#include <QObject>
#include <QVariant>

namespace AutoLinux
{

class DLLEXPORT CppJob : public Job
{
    Q_OBJECT
public:
    explicit CppJob( QObject* parent = nullptr );
    ~CppJob() override;

    void setModuleInstanceKey( const AutoLinux::ModuleSystem::InstanceKey& instanceKey );
    AutoLinux::ModuleSystem::InstanceKey moduleInstanceKey() const { return m_instanceKey; }

    virtual void setConfigurationMap( const QVariantMap& configurationMap );

protected:
    AutoLinux::ModuleSystem::InstanceKey m_instanceKey;
};

}  // namespace AutoLinux

#endif  // AUTOLINUX-INSTALLER_CPPJOB_H
