#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# === This file is part of AutoLinux - <https://autolinux-installer.io> ===
#
#   SPDX-FileCopyrightText: 2014-2015 Philip Müller <philm@manjaro.org>
#   SPDX-FileCopyrightText: 2014 Teo Mrnjavac <teo@kde.org>
#   SPDX-FileCopyrightText: 2017 Alf Gaida <agaida@siduction.org>
#   SPDX-FileCopyrightText: 2017-2018 Gabriel Craciunescu <crazy@frugalware.org>
#   SPDX-FileCopyrightText: 2019 Adriaan de Groot <groot@kde.org>
#   SPDX-License-Identifier: GPL-3.0-or-later
#
#   AutoLinux is Free Software: see the License-Identifier above.
#

import libautolinux-installer

import gettext
_ = gettext.translation("autolinux-installer-python",
                        localedir=libautolinux-installer.utils.gettext_path(),
                        languages=libautolinux-installer.utils.gettext_languages(),
                        fallback=True).gettext


def pretty_name():
    return _("Setting hardware clock.")


def run():
    """
    Set hardware clock.
    """
    hwclock_rtc = ["hwclock", "--systohc", "--utc"]
    hwclock_isa = ["hwclock", "--systohc", "--utc", "--directisa"]
    is_broken_rtc = False
    is_broken_isa = False

    ret = libautolinux-installer.utils.target_env_call(hwclock_rtc)
    if ret != 0:
        is_broken_rtc = True
        libautolinux-installer.utils.debug("Hwclock returned error code {}".format(ret))
        libautolinux-installer.utils.debug("  .. RTC method failed, trying ISA bus method.")
    else:
        libautolinux-installer.utils.debug("Hwclock set using RTC method.")
    if is_broken_rtc:
        ret = libautolinux-installer.utils.target_env_call(hwclock_isa)
        if  ret != 0:
            is_broken_isa = True
            libautolinux-installer.utils.debug("Hwclock returned error code {}".format(ret))
            libautolinux-installer.utils.debug("  .. ISA bus method failed.")
        else:
            libautolinux-installer.utils.debug("Hwclock set using ISA bus method.")
    if is_broken_rtc and is_broken_isa:
        libautolinux-installer.utils.debug("BIOS or Kernel BUG: Setting hwclock failed.")

    return None
