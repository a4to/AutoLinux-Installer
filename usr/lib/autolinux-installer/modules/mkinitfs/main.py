#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# === This file is part of AutoLinux - <https://autolinux-installer.io> ===
#
#   SPDX-FileCopyrightText: 2014-2015 Philip Müller <philm@manjaro.org>
#   SPDX-FileCopyrightText: 2014 Teo Mrnjavac <teo@kde.org>
#   SPDX-FileCopyrightText: 2017 Alf Gaida <agaid@siduction.org>
#   SPDX-FileCopyrightText: 2019 Adriaan de Groot <groot@kde.org>
#   SPDX-License-Identifier: GPL-3.0-or-later
#
#   AutoLinux is Free Software: see the License-Identifier above.
#

import libautolinux-installer
from libautolinux-installer.utils import target_env_call


import gettext
_ = gettext.translation("autolinux-installer-python",
                        localedir=libautolinux-installer.utils.gettext_path(),
                        languages=libautolinux-installer.utils.gettext_languages(),
                        fallback=True).gettext


def pretty_name():
    return _("Creating initramfs with mkinitfs.")


def run_mkinitfs():
    """
    Creates initramfs, even when initramfs already exists.

    :return:
    """
    return target_env_call(['mkinitfs'])


def run():
    """
    Starts routine to create initramfs. It passes back the exit code
    if it fails.

    :return:
    """
    return_code = run_mkinitfs()

    if return_code != 0:
        return ( _("Failed to run mkinitfs on the target"),
                 _("The exit code was {}").format(return_code) )
