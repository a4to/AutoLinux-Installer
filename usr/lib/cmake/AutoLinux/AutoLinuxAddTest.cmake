# === This file is part of AutoLinux - <https://autolinux-installer.io> ===
#
#   SPDX-FileCopyrightText: 2020 Adriaan de Groot <groot@kde.org>
#   SPDX-License-Identifier: BSD-2-Clause
#
#   AutoLinux is Free Software: see the License-Identifier above.
#
#
###
#
# Support functions for building AutoLinux tests.
# This extends KDE's ECM tests with some custom patterns.
#
# autolinux-installer_add_test(
#   <NAME>
#   [GUI]
#   [RESOURCES FILE]
#   SOURCES <FILE..>
#   )

include( CMakeParseArguments )
include( AutoLinuxAutomoc )

function( autolinux-installer_add_test )
    # parse arguments (name needs to be saved before passing ARGN into the macro)
    set( NAME ${ARGV0} )
    set( options GUI )
    set( oneValueArgs NAME RESOURCES )
    set( multiValueArgs SOURCES LIBRARIES DEFINITIONS )
    cmake_parse_arguments( TEST "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )
    set( TEST_NAME ${NAME} )

    if( ECM_FOUND AND BUILD_TESTING )
        ecm_add_test(
            ${TEST_SOURCES} ${TEST_RESOURCES}
            TEST_NAME
                ${TEST_NAME}
            LINK_LIBRARIES
                AutoLinux::autolinux-installer
                ${TEST_LIBRARIES}
                Qt5::Core
                Qt5::Test
            )
        autolinux-installer_automoc( ${TEST_NAME} )
        # We specifically pass in the source directory of the test-being-
        # compiled, so that it can find test-files in that source dir.
        target_compile_definitions( ${TEST_NAME} PRIVATE -DBUILD_AS_TEST="${CMAKE_CURRENT_SOURCE_DIR}"  ${TEST_DEFINITIONS} )
        if( TEST_GUI )
            target_link_libraries( ${TEST_NAME} AutoLinux::autolinux-installerui Qt5::Gui )
        endif()
        if( TEST_RESOURCES )
            autolinux-installer_autorcc( ${TEST_NAME} ${TEST_RESOURCES} )
        endif()
    endif()
endfunction()
