#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "AutoLinux::autolinux-installer" for configuration "Release"
set_property(TARGET AutoLinux::autolinux-installer APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AutoLinux::autolinux-installer PROPERTIES
  IMPORTED_LINK_DEPENDENT_LIBRARIES_RELEASE "Qt5::DBus;kpmcore"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libautolinux-installer.so.3.2.61"
  IMPORTED_SONAME_RELEASE "libautolinux-installer.so.3.2.61"
  )

list(APPEND _cmake_import_check_targets AutoLinux::autolinux-installer )
list(APPEND _cmake_import_check_files_for_AutoLinux::autolinux-installer "${_IMPORT_PREFIX}/lib/libautolinux-installer.so.3.2.61" )

# Import target "AutoLinux::autolinux-installerui" for configuration "Release"
set_property(TARGET AutoLinux::autolinux-installerui APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(AutoLinux::autolinux-installerui PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libautolinux-installerui.so.3.2.61"
  IMPORTED_SONAME_RELEASE "libautolinux-installerui.so.3.2.61"
  )

list(APPEND _cmake_import_check_targets AutoLinux::autolinux-installerui )
list(APPEND _cmake_import_check_files_for_AutoLinux::autolinux-installerui "${_IMPORT_PREFIX}/lib/libautolinux-installerui.so.3.2.61" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
